---
title: "Praxis"
description: ""
images: []
draft: false
menu: main
weight: 3
---

Viele Schulen haben sich bereits entschieden und setzten auf freie Software. Auf dieser Seite zeigen wir ausgewählte Beispiele von Schulen und unterstützenden Projekten.

{{< imgcenter src="/images/greenline.png" >}}

FUSS in Bozen (Südtirol)
========================================================================

{{< imgright src="/images/fuss.png">}}

{{< link href="https://fuss.bz.it" title="FUSS" >}} (Free Upgrade for a digital Sustainable School) hat eine doppelte Natur: Es ist ein digitales Nachhaltigkeitsprojekt für Schulen und gleichzeitig ein freies Software-Produkt (Debian GNU/Linux Betriebssystem und Anwendungen) für ein 360-Grad-Management eines Schulnetzwerks.

FUSS wurde 2005 gegründet und ist aufgrund seiner Organisation und Struktur eine einzigartige Erfahrung in Europa. Das Projekt hat zur Migration von Computerwerkzeugen, die im Unterricht aller Schulen in der italienischen Sprache Südtirols verwendet werden, auf freie Software geführt: Bis heute benutzen es 1.800 Lehrer und 16.000 Schüler; es ist auf 4.000 Desktops und 60 Servern in etwa 80 Schulen in der Autonomen Provinz Bozen installiert). FUSS wird von der italienischen Abteilung für allgemeine und berufliche Bildung der Autonomen Provinz Bozen koordiniert und finanziert. Der derzeitige technologische Partner, der bei der Implementierung und Beratung mitwirkt, ist die italienische Firma Truelite Srl.

Die Entscheidung, freie Software in Schulen einzusetzen, ist in erster Linie eine Entscheidung, die über wirtschaftliche oder technische, ethische und politische Gründe hinausgeht. Neben effizienter, stabiler und sicherer Software ist es das Ziel, die Werte der Freiheit und des Wissensaustauschs im Unterricht zu verfolgen.

FUSS ist ein innovatives Projekt auf dem nationalen Territorium und hat es ermöglicht, den Unterricht dank vier grundlegenden Zielen digital nachhaltig zu gestalten: die Verwendung freier Software, die Verwendung offener Formate, die Schaffung freier Inhalte. Damit wurde der Grundstein für das vierte Ziel gelegt, dessen Erreichung von jeder Schule per Definition garantiert werden sollte: der freie Zugang zum Wissen. Die Philosophie, die hinter der freien Software steht, nämlich der freie Zugang zu Informationen und die gemeinsame Nutzung von Wissen, eignet sich natürlich für die Bildungsaufgabe jeder Schule.

FUSS unterstützt die {{< link href="https://publiccode.eu/" title="Initiative Public Money - Public Code" >}}, die in Italien durch Artikel 69 des Digital Administration Code ratifiziert ist.

Der {{< link href="https://work.fuss.bz.it/projects" title="Quellcode von FUSS" >}} ist frei, öffentlich und transparent, die Dokumentation ist über die {{< link href="https://fuss.bz.it" title="Hauptseite" >}} zugänglich.

{{< imgcenter src="/images/greenline.png" >}}

Georg-Büchner-Gymnasium
========================================================================

{{< imgright src="/images/gymmi.png">}}

Am {{< link href="https://www.gbgseelze.de/" title="Georg-Büchner-Gymnasium" >}} in Seelze bei Hannover gibt es seit einigen Jahren fast nur noch Linux-Geräte. Das Kollegium habe zwar gezögert, dann aber von Windows auf Linux umgestellt, erzählt {{< link href="https://netzpolitik.org/2020/mit-linux-rechnern-zur-digitalen-nachhaltigkeit/" title="Felix Schoppe bei Netzpolitik.org" >}}. Er kümmert sich um die drei Computerräume, mehrere Einzelrechner und insgesamt etwa 150 Laptops für 1.400 Schülerinnen seiner Schule. Eigentlich unterrichtet Felix Schoppe Physik, Französisch und Informatik. Die IT-Arbeit in der Schule ist ihm durch Entlastungsstunden möglich.

Die Schule hat ein Medienbildungskonzept, in dem steht, dass freie Software eingesetzt werden soll. Bei der Abstimmung auf der Gesamtkonferenz gab es keine Gegenstimmen, nur einige Enthaltungen. Auch die Schülervertretung fand den Weg hin zu Linux gut.

Die Schülerinnen arbeiten mit Gebrauchthardware und reparieren diese grösstenteils selbst. Es gibt Notebooks, die über zehn Jahre alt sind, mit Linux aber noch einwandfrei funktionieren. Die Schülerinnen unterstützen Felix Schoppe in Form einer AG und helfen bei der Installation und auch bei der Reparatur defekter Geräte. Dabei ist die Nachhaltigkeit der Hardware ein wesentlicher Aspekt, da bei neuen Geräten meist der Akku oder einzelne Komponenten nicht getauscht werden können.

Obwohl am Gymnasium in Seelze vieles vom Lehrer und der Schülerinnen-AG selbst gemacht wird, empfiehlt Felix Schoppe, einen externen IT-Dienstleister zu beauftragen, der sich um Server, Netzwerk und Reparaturen kümmern. Es gibt Firmen, die sich auf diese Dienstleistung für Schulen spezialisiert haben.

{{< imgcenter src="/images/greenline.png" >}}

Big Blue Button überzeugt in Baden-Württemberg
========================================================================

{{< imgright src="/images/logo-bawue.png">}}

Das Kultusministerium hat den Schulen im Land seit Mitte April 2020 das datenschutzkonforme Videokonferenz-Tool {{< link href="https://bigbluebutton.org/" title="Big Blue Button" >}} für das Fernlernen zur Verfügung gestellt. Interessierte Schulen können seit Anfang Mai Bedarf anmelden und BBB als zusätzliche Funktion in ihrem Moodle-Account freischalten lassen. Etwas 500 Schulen nutzen es bereits. Zusätzlich werden Fortbildungen zu Moodle und Big Blue Button angeboten.

Auf der {{< link href="https://km-bw.de/,Lde/Startseite/Service/2020+06+22+Big+Blue+Button+und+Fortbildungsangebote" title="Seite des Kultusministeriums" >}} finden sich interessante Details zum Einsatz von BBB an Schulen, sowie Fortbildungsangebote. Die positiven Rückmeldungen sprechen für sich:

„Am Bodensee wird der Button schon emsig eingesetzt. Endlich gibt es eine datenschutzverträgliche offizielle Lösung für Webkonferenzen.“ Sowie: „Nachdem BBB und Moodle als geniales Team im Wesentlichen super läuft und wir damit in Baden-Württemberg einen Einstieg in selbst gehostete und damit souveräne Lernplattformen mit Open Source hingelegt haben, möchte ich im Namen meines ganzen Kollegium aus ganzem Herzen ein ganz großes Dankeschön sagen.“

{{< imgcenter src="/images/greenline.png" >}}

Base4Kids2 - Eine umfassende Lösung für Berner Schulen
========================================================================

{{< imgright src="/images/base4kids.png">}}

Das Projekt {{< link href="https://base4kids.ch/base4kids2/" title="base4kids2" >}} ist die Antwort der Stadtberner Schulen, um den neuen Anforderungen des {{< link href="https://www.lehrplan21.ch/" title="Lehrplan21" >}} und der unaufhaltsam fortschreitenden Digitalisierung gerecht zu werden. Es ist eine Open Source geprägte Lösung, welche den Lehrpersonen ermöglicht, den Schulalltag effizienter, vielseitiger und individueller zu gestalten.

Das Stadtberner Schulinformatikprojekt Base4kids 2 ist laut dem Verein CH Open in Schieflage geraten. Schuld daran sei aber nicht die Open-Source-Software, deren Einsatz in die Kritik geraten ist. Mehr dazu finden sie in {{< link href="https://www.itmagazine.ch/Artikel/73509/Maengel_bei_Base4kids_2_Open_Source_ist_nicht_schuld_sagt_CH_Open.html" title="diesem Artikel" >}}.  


{{< imgcenter src="/images/greenline.png" >}}

Schule Saanen setzt auf Open Source
========================================================================

{{< imgright src="/images/schulesaanen.jpg">}}

Die Bildungskommission der Schulen der Gemeinde Saanen hat den richtungsweisenden Entschluss gefasst, auf Open Source Lösungen im Office-Bereich und beim Dateiaustausch zu setzen. Sämtliche Schüler und Lehrpersonen werden in Zukunft Libre Office und den {{< link href="https://www.openeduserver.ch/" title="Open Education Server" >}} (basierend auf Nextcloud) von CH Open nutzen. In diesem Zusammenhang wurde der Forschungsstelle für Digitale Nachhaltigkeit, welche den Open Education Server entwickelt hat und betreibt, der Auftrag erteilt, für die Schulen Saanen mehrere vertiefte Workshops für LibreOffice und zum Open Education Server durchzuführen.
