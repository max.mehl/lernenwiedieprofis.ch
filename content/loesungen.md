---
title: "Lösungen"
description: ""
images: []
draft: false
menu: main
weight: 2
---

Schulen brauchen keine theoretischen Erklärungen, sondern praktische Vorschläge und Ideen. Dennoch ist das Verständnis der Situation und der Zusammenhänge wichtig für richtige Entscheidungen. In unserem Kapitel Lösungen haben wir viele konkrete Vorschläge beschrieben, wie der Schulbetrieb mit Freier Software effizient und einfach gestaltet werden kann.

- {{< anchor href="#kommunikation" title="Kommunikation" >}}  
- {{< anchor href="#infrastruktur" title="Infrastruktur" >}}  
- {{< anchor href="#schulinformationssysteme" title="Schulinformationssysteme" >}}  
- {{< anchor href="#lernmanagementsysteme" title="Lernmanagementsysteme" >}}  
- {{< anchor href="#zusammenarbeiten" title="Zusammenarbeiten" >}}  
- {{< anchor href="#office" title="Office" >}}  
- {{< anchor href="#multimedia" title="Multimedia" >}}  
- {{< anchor href="#sonstige-werkzeuge" title="Sonstige Werkzeuge" >}}  
- {{< anchor href="#nützliche-links" title="Nützliche Links" >}}  

{{< imgcenter src="/images/greenline.png" >}}


Kommunikation
================================================================================

Nicht nur zu Zeiten einer Pandemie, steht die Kommunikation zwischen Menschen an erster Stelle der Bedürfnisse. Sei es der Austausch mit der Familie und Freunden, oder die Arbeit aus dem Homeoffice und der Fernunterricht für Schülerinnen und Studenten. Aus der Not geboren lassen viele von uns ihre Bedenken fahren und verwenden Lösungen, die der Privatsphäre und dem Datenschutz diametral gegenüberstehen. Facebook, WhatsApp, Microsoft Team und Zoom sind keine Lösungen, sondern der Ausverkauf unserer Grundrechte. Deshalb schlagen wir hier wirkliche Lösungen vor.

**BigBlueButton**

{{< imgright src="/images/bbb_logo.png">}}

{{< link href="https://bigbluebutton.org/" title="BigBlueButton" >}} ermöglicht die gemeinsame Nutzung von Audio, Video, Folien, Chat und Bildschirm in Echtzeit. Die Studentinnen werden durch die gemeinsame Nutzung von Emoji-Symbolen, Abstimmungen und Breakout-Räumen eingebunden.

Öffentliche BBB-Server gibt es hier: 
{{< link href="https://www.senfcall.de/" title="Senfcall" >}},
{{< link href="https://bbb.ch-open.ch/b" title="CH-Open" >}}

**Jitsi**

{{< link href="https://jitsi.org/" title="Jitsi" >}} (bulgarisch für Drähte) ist eine Sammlung von freier Software für IP-Telefonie (VoIP), Videokonferenzen und Instant Messaging. Inzwischen gibt es mit Jitsi Meet eine Software für Videokonferenzen, die im Webbrowser, als mobile App und Anwendungssoftware für Windows, MacOS und Linux genutzt werden kann. Im Zuge der Schulschliessungen ab Mitte März 2020 aufgrund des Corona-Virus gewann Jitsi viele neue Nutzerinnen durch Online-Unterricht, da es ohne Anmeldung und ohne Installation verwendet werden kann.

Öffentliche Jitsi-Server gibt es hier:
{{< link href="https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances" title="Jitsi Server" >}}

**Matrix**

{{< link href="https://matrix.org/" title="Matrix" >}} ist ein offenes Protokoll für Echtzeitkommunikation. Es wurde entworfen, um Benutzern mit Konten bei einem Matrix-Diensteanbieter zu erlauben, mit Benutzern anderer Matrix-Diensteanbieter per Chat, IP-Telefonie und Video-Telefonie zu kommunizieren. Dabei handelt es sich um ein dezentralisiertes und föderales System (ähnlich wie E-Mail), bei dem jeder einen Matrix-Server betreiben kann. Matrix unterstützt Ende-zu-Ende Verschlüsselung sowohl bei der Peer-to-Peer-Kommunikation als auch in Gruppen. Für Matrix gibt es viele verschiedene {{< link href="https://matrix.org/clients-matrix/" title="Clients" >}} für alle Betriebssysteme.

Die Auswahl an Matrix-Server-Betreibern ist riesig: {{< link href="https://www.hello-matrix.net/public_servers.php" title="Hello Matrix" >}},  {{< link href="https://publiclist.anchel.nl/" title="Publiclist Anchel" >}}, {{< link href="https://the-federation.info/protocol/matrix" title="The Federation" >}}.

**Weitere nennenswerte Produkte sind:**

- {{< link href="https://www.mumble.com/" title="Mumble" >}}, {{< link href="https://www.mumble.com/serverlist/" title="Mumble Server" >}} (nur Audio)
- {{< link href="https://xmpp.org/" title="XMPP" >}}, {{< link href="https://list.jabber.at/" title="XMPP Server" >}} (Chat)
- {{< link href="https://jami.net/" title="GNU Jami" >}} (Chat, Audio, Video)
- {{< link href="https://nextcloud.org" title="Nextcloud Talk" >}} (Chat, Audio, Video)
- {{< link href="https://rocket.chat/" title="Rocket Chat" >}} (Chat, Audio, Video)
- {{< link href="https://jam.systems/" title="Jam.Systems" >}} (ähnlich wie {{< link href="https://de.wikipedia.org/wiki/Clubhouse_(App)" title="Clubhouse" >}})
- {{< link href="https://mattermost.com/" title="Mattermost" >}} (Chat, Audio, Video)
- {{< link href="https://zulip.com/" title="Zulip" >}}

{{< imgcenter src="/images/greenline.png" >}}


Infrastruktur
================================================================================

**DebianEdu**

{{< imgright src="/images/debianedu.png">}}

{{< link href="https://www.skolelinux.de/de/" title="DebianEdu" >}} (Skolelinux) ist eine Anpassung der Linux-Distribution Debian für schulische Bedürfnisse. Das internationale Projekt wurde 2001 in Norwegen gegründet und liefert auf einer einzigen Installations-CD alles um ein typisches Schulnetzwerk zu betreiben. Auch ungeschulte Anwender können in wenigen Schritten sowohl einen Kommunikationsserver mit LDAP-Nutzerdatenbank, einen Terminalserver, eine Arbeitsstation oder ein Notebook als Einzelplatzrechner installieren. Dabei kommt ausschliesslich freie Software zum Einsatz, darunter auch viel Lernsoftware.

Die Schulnetzwerk-Lösung für alle

- Server, Desktops, Thin Clients - alles aus einem Guss
- Kostenlos, aber mit kommerziellem und professionellem Support
- 100% Freie Software - Einsatzbereit für jeden Zweck
- Volle Kompatibilität zu Microsoft-Windows-Clients
- Umfangreiche Softwarepakete für Unterricht, Verwaltung und mehr
- Sofort einsatzfähiger, stabiler, sicherer Hauptserver
- Einfach installierbares, voll konfiguriertes Netzwerk, fertige Terminalserver, Clients und Workstations

{{< imgcenter src="/images/greenline.png" >}}


Schulinformationssysteme
========================================================================

**Aleksis**

{{< imgright src="/images/aleksis.svg">}}

{{< link href="https://aleksis.org/" title="Aleksis" >}} ist ein webbasiertes Schulinformationssystem (SIS) das zur Verwaltung und/oder Veröffentlichung organisatorischer Themen von Bildungseinrichtungen verwendet werden kann. Die Anwendungen bieten eine reichhaltige Funktionalität für diverse schulische Einsatzgebiete, wie die Verwaltung von Schülern und Lehrern, Stundenplänen und Vertretungen, Klassenregistern, persönlichen Notizen, Hausaufgaben, Aufgaben und vieles mehr. Neue Apps werden regelmässig hinzugefügt, um den Funktionsumfang von AlekSIS zu erweitern. 


**Orange HRM**

{{< link href="https://www.orangehrm.com/" title="Orange HRM" >}} ist ein umfassendes Human Resource Management-System das die wesentlichen Funktionalitäten bietet, die für jede Schule erforderlich sind.


**InvoiceNinja**

{{< link href="https://www.invoiceninja.org/" title="InvoiceNinja" >}} dient dem Erstellen und Versenden von Rechnungen und Offerten, Mahnwesen, Ausgaben sowie Zeiterfassung und Projektabwicklung.


**Weitere nennenswerte Produkte sind:**

{{< link href="https://github.com/InvoicePlane/InvoicePlane" title="InvoicePlane" >}}

{{< imgcenter src="/images/greenline.png" >}}


Lernmanagementsysteme
========================================================================

**Moodle**

{{< imgright src="/images/moodle.svg">}}

{{< link href="https://moodle.org/" title="Moodle" >}} ist eine freies Online-Lernmanagementsystem, das Lehrenden die Möglichkeit gibt, Webseiten mit dynamischen Lerninhalten bereitzustellen und damit zeit- und ortsunabhängiges Lernen zu ermöglichen. Egal, ob Sie lehren, lernen oder administrative Aufgaben im Bildungskontext wahrnehmen. Die Integration mit dem Videochat-System {{< link href="https://bigbluebutton.org/" title="BigBlueButton" >}} ist ebenfalls möglich.

Moodle bietet:

- eine moderne, leicht bedienbare Oberfläche
- ein personalisiertes Dashboard
- Funktionen für Kommunikation und Kollaboration
- den Alles-in-einem-Kalender
- bequeme Dateiverwaltung
- einen intuitiven Text-Editor
- Mitteilungen an alle Benutzerinnen
- Verfolgen des Lernfortschritts
- und vieles mehr für Administratoren, das Lehrpersonal und SchülerInnen


**Mahara**

- {{< link href="https://mahara.org/" title="Mahara" >}} ist ein freies und quelloffenes webbasiertes elektronisches Portfoliomanagementsystem.


**Weitere nennenswerte Produkte sind:**

- {{< link href="https://chamilo.org/en/" title="Chamilo" >}}
- {{< link href="https://www.edx.org/" title="EdX" >}}
- {{< link href="https://www.ilias.de/" title="Ilias" >}}

{{< imgcenter src="/images/greenline.png" >}}


Zusammenarbeiten
========================================================================

**Nextcloud**

{{< imgright src="/images/Nextcloud_Logo.svg">}}

{{< link href="https://nextcloud.com/de/" title="Nextcloud" >}} ist die führende freie Produktivitätsplattform und die erste Wahl im Umfeld von Behörden und Organisationen. Nextcloud bietet eine grosse Vielfalt an Apps für die Zusammenarbeit von Schülerinnen und dem Lehrpersonal. Dies reicht von der Dateiablage, Fotogalerien über Kalender und Kontaktlisten bis zur gemeinsamen Planung von Schulprojekten.

Die Anwendung "Documents" unterstützt die Bearbeitung von Texten innerhalb von Nextcloud, ohne dass eine externe Anwendung gestartet werden muss. Folgende Funktionen werden unterstützt:

- Kooperatives Bearbeiten, wobei mehrere Benutzer gleichzeitig Dateien editieren können
- Erstellung von Dokumenten innerhalb von Nextcloud
- Hochladen von Dokumenten
- Teilen von Dateien über einen Link


**CodiMD**

{{< link href="https://demo.codimd.org/" title="CodiMD" >}} ist ein in Echtzeit arbeitender, plattformübergreifender Editor für Dokumente und Notizen im Markdown Format.


**Cryptpad**

{{< link href="https://cryptpad.fr/" title="Cryptpad" >}} ist ein Browser-basierter, verschlüsselter, kollaborativer Echtzeit-Editor. Dokumente können im Browser gleichzeitig mit Drittpersonen bearbeitet und deren Änderungen sofort eingesehen werden.


**Weitere nennenswerte Produkte sind:**

- {{< link href="https://drawpile.net" title="DrawPile" >}}
- {{< link href="https://www.dokuwiki.org" title="Dokuwiki" >}}
- {{< link href="https://etherpad.org/" title="Etherpad Lite" >}}
- {{< link href="https://ethercalc.net/" title="Ethercalc" >}}

{{< imgcenter src="/images/greenline.png" >}}


Office
========================================================================

**Libreoffice**

{{< imgright src="/images/LibreOffice.png">}}

{{< link href="https://de.libreoffice.org" title="Libreoffice" >}} ist ein leistungsstarkes Office-Paket. Die klare Oberfläche und mächtigen Werkzeuge lassen Sie Ihre Kreativität entfalten und Ihre Produktivität steigern. LibreOffice vereint verschiedene Anwendungen wie Textverarbeitung, Tabellenkalkulation, Präsentationen und Datenbanken. Das macht es zum überzeugendsten freien und quelloffenen Office-Paket auf dem Markt. Selbst proprietäre Formate von Microsoft Office können gelesen und gespeichert werden.


**Thunderbird**

{{< link href="https://www.thunderbird.net/de" title="Thunderbird" >}} ist eine freie E-Mail-Anwendung, die sich leicht einrichten lässt und über viele zusätzliche Funktionen verfügt.


**Weitere nennenswerte Produkte sind:**

- {{< link href="https://www.abisource.com/" title="AbiWord" >}}
- {{< link href="https://wiki.gnome.org/Apps/Evince/Downloads" title="Evince" >}}
- {{< link href="http://www.gnumeric.org/" title="Gnumeric" >}}
- {{< link href="https://wiki.gnome.org/Apps/Evolution" title="Evolution" >}}
- {{< link href="https://github.com/xournalpp/xournalpp" title="Xournal++" >}}

{{< imgcenter src="/images/greenline.png" >}}


Multimedia
========================================================================

**VLC**

{{< imgright src="/images/vlc.svg">}}

{{< link href="https://www.videolan.org/" title="VLC" >}} ist ein freier und quelloffener, plattformübergreifender Multimedia-Player, der die meisten Multimedia-Dateien sowie DVDs, Audio-CDs, VCDs und verschiedene Streaming-Protokolle abspielt.


**Weitere nennenswerte Produkte sind:**

- {{< link href="https://inkscape.org/de/" title="Inkscape" >}} ist ein vektorbasiertes Grafikprogramm.
- {{< link href="http://ampache.org/" title="Ampache" >}} streamt eure Musik.
- {{< link href="https://www.gimp.org/" title="GIMP" >}} ist der Standard für Fotobearbeitung.
- {{< link href="https://www.audacityteam.org/" title="Audacity" >}} erstellt und bearbeitet Audio-Dateien.
- {{< link href="https://www.openshot.org/de/" title="Openshot" >}} ist die preisgekrönte Videobearbeitung.

{{< imgcenter src="/images/greenline.png" >}}


Sonstige Werkzeuge
========================================================================

{{< imgright src="/images/peertube.png">}}

- {{< link href="https://joinpeertube.org/en/" title="Peertube" >}} ist eine freie Video Plattform.
- {{< link href="https://calibre-ebook.com/" title="Calibre" >}} das beste Tool für Ebooks.
- {{< link href="https://www.openstreetmap.org" title="Openstreetmap" >}} die aktuellsten Karten dank Crowd-Sourcing.
- {{< link href="https://framadate.org" title="Framadate" >}} erleichtert die Terminfindung und das Erstellung von Umfragen.
- {{< link href="https://remmina.org/" title="Remmina" >}} macht die Fernwartung von anderen PCs einfach.
- {{< link href="https://gnulinux.ch/gnome-boxes" title="GNOME-Boxes" >}} Distros ausprobieren und mit anderen PCs verbinden.
- {{< link href="https://www.savapage.org/" title="SavaPage" >}} ist ein freies Print-Portal.

{{< imgcenter src="/images/greenline.png" >}}


Nützliche Links
========================================================================

- {{< link href="https://www.ossdirectory.com/oss-firmen/" title="OSS Directory Firmen" >}} - Open Source Anbieterverzeichnis
- {{< link href="https://www.ossdirectory.com/oss-produkte/" title="OSS Directory Produkte" >}} - Open Source Produktverzeichnis
- {{< link href="https://openeduserver.ch/" title="Open Education Server" >}} - kostenlose Angebote für Lehrpersonen
- {{< link href="https://openeducationday.ch/" title="Open Education Day" >}} - Tagung rund um Open Source und Bildung
- {{< link href="https://www.digitale-gesellschaft.ch/" title="Digitale Gesellschaft" >}} - Verein für Bürger- und Konsumenten­schutz im digitalen Zeitalter
- {{< link href="https://digitalcourage.de/blog/2020/freie-software-fuer-schulen" title="Digital Courage" >}} - Freie Software für Schulen
- {{< link href="https://www.stiftung-mercator.ch/de/aktuelles/videokonferenzen-mit-jitsi-server-tipps-und-erfahrungen/" title="Mercator Stiftung" >}} - Förderung von Projekten zu gesellschaftlichen Fragen
- {{< link href="http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/einleitung-werkzeugkasten-freie-software/" title="Medien in die Schule" >}} - Einleitung Werkzeugkasten Freie Software
