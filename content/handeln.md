---
title: "Jetzt handeln"
description: ""
images: []
draft: false
menu: main
weight: 4
---

Freie Software für eine freie Gesellschaft fällt nicht vom Himmel. Hierfür ist
das Engagement der Zivilgesellschaft erforderlich. Das sind die Eltern, die Schüler
und die Lehrpersonen.
{{< imgcenter src="/images/school_1.jpg" >}}  


Um ihr Handeln so einfach wie möglich zu gestalten, haben wir einen Musterbrief
entworfen, der an Schulen, Universitäten und Behörden gesendet werden kann. Der
Brief formuliert die Anliegen für ein freies Lernen in Bildungseinrichtungen und
den Schutz von Schülern, Schülerinnen und allen Lernenden vor den Übergriffen der
globalen Konzerne (Google, Apple, Microsoft) auf die persönlichen Daten.

Zusätzlich haben wir eine einfache Möglichkeit eingefügt, mit der sie eine
Auskunft der über sie gespeicherten Daten von Firmen anfordern können.

{{< imgcenter src="/images/greenline.png" >}}


Jetzt aktiv werden
================================================================================

Schreiben sie einen Brief an ihre Schule, Universität oder Schulbehörde. Hier ist der
Musterbrief:

    Betreff: Einsatz von Freier Software im Unterricht

    Sehr geehrte XY

    Aus dem Schweizer Lehrplan21 geht hervor, dass SchülerInnen den Aufbau und die Funktionsweise von informationsverarbeitenden Systemen kennen und Konzepte der sicheren Datenverarbeitung anwenden können sollen.

    Leider wird an Schulen vorwiegend mittels unfreier Software gelehrt. Diese bietet den SchülerInnen und Lehrpersonen keine Möglichkeit, die Funktionsweise und Grundsätze der Programme zu studieren, sondern erlaubt lediglich deren Nutzung.

    Primäres Ziel des Lehrplan21 ist jedoch nicht, bestimmte Software-Produkte zu bewerben, sondern SchülerInnen grundlegende informationstechnische Kompetenzen zu vermitteln.

    Aus diesem Grunde würde ich es begrüssen, wenn an dieser Bildungseinrichtung nicht nur der Umgang mit proprietären Systemen (Microsoft, Apple, etc.) unterrichtet wird, sondern alternative Ansätze aufgezeigt werden und die digitale Mündigkeit und Souveränität der SchülerInnen, aber auch der Lehrpersonen, mittels Freier Software gefördert werden. Dadurch kann vermieden werden, dass junge Menschen früh und unkritisch auf ganz bestimmte kommerzielle Produkte geschult werden.

    Auf https://www.lernenwiedieprofis.ch stehen Ihnen weitere Informationen zu diesem Thema zur Verfügung.

    Ich danke Ihnen im Voraus für die Berücksichtigung meines Anliegens.

    Freundliche Grüsse

    Ihre Unterschrift


Sie können diesen Text in eine E-Mail kopieren und an Bildungseinrichtung
versenden.

{{< imgcenter src="/images/greenline.png" >}}


Es geht noch einfacher
================================================================================

Falls sie den Text per E-Mail versenden möchten, können sie auch auf diesen Link klicken:

{{< briefmail >}}

Es öffnet sich dann ihr E-Mail Programm, in dem sie die Adresse, die Anrede
und ihre Unterschrift einfügen können.

{{< imgcenter src="/images/greenline.png" >}}


Flyer zum Verteilen
================================================================================

Neben dem Brief gibt es auch einen zweiseitigen Flyer zum Verteilen.
{{< pdflink src="/pdf/Flyer_LwdP.pdf" title="Flyer als PDF-Datei">}}

{{< imgcenter src="/images/flyer_lwdp.png" >}}


{{< imgcenter src="/images/greenline.png" >}}


Datenauskunftsbegehren
================================================================================

Die {{< link href="https://www.digitale-gesellschaft.ch/" title="Digitale Gesellschaft Schweiz" >}} hat ein Formular entworfen, mit der man
mit wenigen Angaben ein Datenauskunftsbegehren an jede Firma, Webseiten-Betreiber
oder Behörde stellen kann. Damit erfährt man, welche Daten über die eigene Person
dort hinterlegt sind:

- {{< link href="https://www.digitale-gesellschaft.ch/auskunftsbegehren" title="Klicke hier und fordere jetzt deine Daten ein!" >}}

Jetzt handeln - erstelle ein Datenauskunftsbegehren.

{{< imgcenter src="/images/greenline.png" >}}

{{< center >}}

Herzlichen Dank für ihr Handeln.

