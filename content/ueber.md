---
title: "Über"
description: ""
images: []
draft: false
menu: main
weight: 6
---


Die «Free Software Foundation Europe - Lokalgruppe Zürich» ist eine lokale Unterorganisation der {{< link href="https://fsfe.org" title="FSFE" >}}. Wir setzen uns für die Information und Verbreitung von Freier Software in Unternehmen, Behörden, Organisationen und in der Zivilgesellschaft ein. Dazu bieten wir Informationsmaterial, organisieren Veranstaltungen und halten Vorträge.

Jeden zweiten Donnerstag im Monat, treffen wir uns um die aktuellen Massnahmen zu organisieren. Interessenten sind herzlich willkommen um uns zu unterstützen.

Gestaltung der Webseite: ralfhersel@fsfe.org
