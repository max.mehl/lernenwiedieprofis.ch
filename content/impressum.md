---
title: "Impressum"
description: ""
images: []
draft: false
menu: main
weight: 5
---

Wer sind wir?
========================================================================

Diese Webseite und deren Inhalt wird durch eine private Personengruppe ohne kommerzielle Interessen zur Verfügung gestellt. Verantwortliche Stelle im Sinne der Datenschutzgesetze, insbesondere der EU-Datenschutzgrundverordnung (DSGVO), lautet:

Gian-Maria Daffré, Loostrasse 21, CH-8803 Rüschlikon  
E-Mail: info@lernenwiedieprofis.ch  
Webseite: www.lernenwiedieprofis.ch

{{< imgcenter src="/images/greenline.png" >}}

Ihre Betroffenenrechte
========================================================================

Unter den angegebenen Kontaktdaten unseres Datenschutzbeauftragten können Sie jederzeit folgende Rechte ausüben:

- Auskunft über Ihre bei uns gespeicherten Daten und deren Verarbeitung, Berichtigung unrichtiger personenbezogener Daten,
- Löschung Ihrer bei uns gespeicherten Daten,
- Einschränkung der Datenverarbeitung, sofern wir Ihre Daten aufgrund gesetzlicher Pflichten noch nicht löschen dürfen,
- Widerspruch gegen die Verarbeitung Ihrer Daten bei uns und Datenübertragbarkeit, sofern Sie in die Datenverarbeitung eingewilligt haben oder einen Vertrag mit uns abgeschlossen haben.
- Sofern Sie uns eine Einwilligung erteilt haben, können Sie diese jederzeit mit Wirkung für die Zukunft widerrufen.

{{< imgcenter src="/images/greenline.png" >}}

Zwecke der Datenverarbeitung durch die verantwortliche Stelle und Dritte
========================================================================

Wir verarbeiten Ihre personenbezogenen Daten nur zu den in dieser Datenschutzerklärung genannten Zwecken. Eine Übermittlung Ihrer persönlichen Daten an Dritte zu anderen als den genannten Zwecken findet nicht statt. Wir geben Ihre persönlichen Daten nur an Dritte weiter, wenn:

- Sie Ihre ausdrückliche Einwilligung dazu erteilt haben,
- die Verarbeitung zur Abwicklung eines Vertrags mit Ihnen erforderlich ist,
- die Verarbeitung zur Erfüllung einer rechtlichen Verpflichtung erforderlich ist,
- die Verarbeitung zur Wahrung berechtigter Interessen erforderlich ist und kein Grund zur Annahme besteht, dass Sie ein überwiegendes schutzwürdiges Interesse an der Nichtweitergabe Ihrer Daten haben.

{{< imgcenter src="/images/greenline.png" >}}

Löschung bzw. Sperrung der Daten
========================================================================

Wir halten uns an die Grundsätze der Datenvermeidung und Datensparsamkeit. Wir speichern Ihre personenbezogenen Daten daher nur so lange, wie dies zur Erreichung der hier genannten Zwecke erforderlich ist oder wie es die vom Gesetzgeber vorgesehenen vielfältigen Speicherfristen vorsehen. Nach Fortfall des jeweiligen Zweckes bzw. Ablauf dieser Fristen werden die entsprechenden Daten routinemässig und entsprechend den gesetzlichen Vorschriften gesperrt oder gelöscht.

{{< imgcenter src="/images/greenline.png" >}}

SSL-Verschlüsselung
========================================================================

Um die Sicherheit Ihrer Daten bei der Übertragung zu schützen, verwenden wir dem aktuellen Stand der Technik entsprechende Verschlüsselungsverfahren (z. B. SSL) über HTTPS.

{{< imgcenter src="/images/greenline.png" >}}

Kontakt zu uns
========================================================================

Treten Sie bzgl. Fragen jeglicher Art per E-Mail oder Kontaktformular mit uns in Kontakt, erteilen Sie uns zum Zwecke der Kontaktaufnahme Ihre freiwillige Einwilligung. Hierfür ist die Angabe einer validen E-Mail-Adresse erforderlich. Diese dient der Zuordnung der Anfrage und der anschliessenden Beantwortung derselben. Die Angabe weiterer Daten ist optional. Die von Ihnen gemachten Angaben werden zum Zwecke der Bearbeitung der Anfrage sowie für mögliche Anschlussfragen gespeichert. Nach Erledigung der von Ihnen gestellten Anfrage werden personenbezogene Daten automatisch gelöscht.

{{< imgcenter src="/images/greenline.png" >}}

Erfassung allgemeiner Informationen beim Besuch unserer Website
========================================================================

Wenn Sie auf unsere Website zugreifen, werden automatisch mittels eines Cookies Informationen allgemeiner Natur erfasst. Diese Informationen (Server-Logfiles) beinhalten etwa die Art des Webbrowsers, das verwendete Betriebssystem, den Domainnamen Ihres Internet-Service-Providers und ähnliches. Hierbei handelt es sich ausschliesslich um Informationen, welche keine Rückschlüsse auf Ihre Person zulassen.

Diese Informationen sind technisch notwendig, um von Ihnen angeforderte Inhalte von Webseiten korrekt auszuliefern und fallen bei Nutzung des Internets zwingend an. Sie werden insbesondere zu folgenden Zwecken verarbeitet:

- Sicherstellung eines problemlosen Verbindungsaufbaus der Website,
- Sicherstellung einer reibungslosen Nutzung unserer Website,
- Auswertung der Systemsicherheit und -stabilität sowie zu weiteren administrativen Zwecken.

Die Verarbeitung Ihrer personenbezogenen Daten basiert auf unserem berechtigten Interesse aus den vorgenannten Zwecken zur Datenerhebung. Wir verwenden Ihre Daten nicht, um Rückschlüsse auf Ihre Person zu ziehen. Empfänger der Daten sind nur die verantwortliche Stelle und ggf. Auftragsverarbeiter.

Anonyme Informationen dieser Art werden von uns ggfs. statistisch ausgewertet, um unseren Internetauftritt und die dahinterstehende Technik zu optimieren.

{{< imgcenter src="/images/greenline.png" >}}

Cookies
========================================================================

Wie viele andere Webseiten verwenden wir auch so genannte „Cookies“. Cookies sind kleine Textdateien, die von einem Webseitenserver auf Ihre Festplatte übertragen werden. Hierdurch erhalten wir automatisch bestimmte Daten wie z. B. IP-Adresse, verwendeter Browser, Betriebssystem über Ihren Computer und Ihre Verbindung zum Internet.

Anhand der in Cookies enthaltenen Informationen können wir Ihnen die Navigation erleichtern und die korrekte Anzeige unserer Webseiten ermöglichen.

In keinem Fall werden die von uns erfassten Daten an Dritte weitergegeben oder ohne Ihre Einwilligung eine Verknüpfung mit personenbezogenen Daten hergestellt.

Natürlich können Sie unsere Website grundsätzlich auch ohne Cookies betrachten. Internet-Browser sind regelmässig so eingestellt, dass sie Cookies akzeptieren. Sie können die Verwendung von Cookies jederzeit über die Einstellungen Ihres Browsers deaktivieren. Bitte verwenden Sie die Hilfefunktionen Ihres Internetbrowsers, um zu erfahren, wie Sie diese Einstellungen ändern können. Bitte beachten Sie, dass einzelne Funktionen unserer Website möglicherweise nicht funktionieren, wenn Sie die Verwendung von Cookies deaktiviert haben.

{{< imgcenter src="/images/greenline.png" >}}

Server Logfiles
========================================================================

Logfile-Informationen werden aus Sicherheitsgründen (z.B. zur Aufklärung von Missbrauchs- oder Betrugshandlungen) für einen kurzen Zeitraum gespeichert und danach gelöscht. Daten, deren weitere Aufbewahrung zu Beweiszwecken erforderlich ist, sind bis zur endgültigen Klärung des jeweiligen Vorfalls von der Löschung ausgenommen.

{{< imgcenter src="/images/greenline.png" >}}

Verwendung von Matomo
========================================================================

Diese Website benutzt Matomo (ehemals Piwik), eine Open-Source-Software zur statistischen Auswertung von Besucherzugriffen. Matomo verwendet sog. Cookies, also Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen.

Die durch das Cookie erzeugten Informationen über Ihre Nutzung des Internetangebotes werden auf einem Server in Deutschland gespeichert.

Die IP-Adresse wird unmittelbar nach der Verarbeitung und vor deren Speicherung anonymisiert. Sie haben die Möglichkeit, die Installation der Cookies durch Änderung der Einstellung Ihrer Browser-Software zu verhindern. Wir weisen Sie darauf hin, dass bei entsprechender Einstellung eventuell nicht mehr alle Funktionen dieser Website zur Verfügung stehen.

Sie können sich entscheiden, ob in Ihrem Browser ein eindeutiger Webanalyse-Cookie abgelegt werden darf, um dem Betreiber der Webseite die Erfassung und Analyse verschiedener statistischer Daten zu ermöglichen.

{{< imgcenter src="/images/greenline.png" >}}

FontAwesome
========================================================================

Wir setzen FontAwesome zur visuellen Gestaltung unserer Website ein. FontAwesome ist eine freie Schriftartenbibliothek (SIL OFL 1.1 License). Zur Einbindung der von uns benutzten Schriftarten, muss Ihr Browser eine Verbindung zu einem Server in den USA aufbauen und die für unsere Website benötigte Schriftart herunterladen. Datenschutzerklärung: {{< link href="https://fontawesome.com/privacy" title="Fontawesome" >}}

{{< imgcenter src="/images/greenline.png" >}}

Änderung unserer Datenschutzbestimmungen
========================================================================

Wir behalten uns vor, diese Datenschutzerklärung anzupassen, damit sie stets den aktuellen rechtlichen Anforderungen entspricht oder um Änderungen unserer Leistungen in der Datenschutzerklärung umzusetzen, z.B. bei der Einführung neuer Services. Für Ihren erneuten Besuch gilt dann die neue Datenschutzerklärung.

{{< imgcenter src="/images/greenline.png" >}}

Fragen an den Datenschutzbeauftragten
========================================================================

Wenn Sie Fragen zum Datenschutz haben, schreiben Sie uns bitte eine E-Mail oder wenden Sie sich direkt an die für den Datenschutz verantwortliche Person in unserer Organisation:

Gian-Maria Daffré, Loostrasse 21, CH-8803 Rüschlikon

info(at)lernenwiedieprofis.ch
